module.exports = {
  apps : [{
    name: "middleware",
    script: "/var/www/html/middleware/index.js",
    output: "/var/www/html/middleware/forever_log/outfile.txt",
    logfile: "/var/www/html/middleware/forever_log/logfile.txt",
    error: "/var/www/html/middleware/forever_log/err.txt",
    watch: true,
    env: {
      NODE_ENV: "development",
    },
    env_production : {
       NODE_ENV: "production"
    }
  }]
}

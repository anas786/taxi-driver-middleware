// rideController.js - Ride Controller module.
var config = require('../../config');
var request = require('request');
var bodyParser = require('body-parser');

// Require model modules.
var ride_model = require('../models/ride');
var driver_model = require('../models/driver');
var passenger_model = require('../models/passenger');
var vehicle_type_model = require('../models/vehicle_type');

// Index Function
exports.index = function(req, res) {
    res.send('Invalid method');
}

// Book Ride Function
exports.bookRide = function(req, res) {
    if( req.passenger_id != null && req.passengers != null && req.vehicle_type_id != null && req.pickup_coordinates != null && req.pickup_location != null && req.dropoff_coordinates != null && req.dropoff_location != null && req.payment_method != null && req.ride_type != null && req.estimated_price != null && req.estimated_duration != null && req.estimated_distance != null ) {
        ride_model.bookRide(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                // Record inserted, now find nearby drivers
                driver_model.findNearbyDrivers(data, function(err, dat) {
                    if( err ) {
                        return res(err);
                    } else {
                        var response = {
                            'socket_ids': dat,
                            'ride_detail': data
                        };
                        return res(null, response);
                    }
                });
            }
        });
    } else {
        return res("Bad request");
    }

    /*var options = {
        host: config.API_SERVER,
        port: config.API_PORT,
        path: config.API_PATH + '/driver/options?driver_id=3',
        method: 'GET'
    }
    http.request(options, function(resp) {
        //console.log('STATUS: ' + res.statusCode);
        //console.log('HEADERS: ' + JSON.stringify(res.headers));
        resp.setEncoding('utf8');
        resp.on('data', function (chunk) {
            //console.log('BODY: ' + chunk);
            res.setHeader('Content-Type', 'application/json');
            res.send(chunk);
        });
        resp.on('end', function () {
            //console.log('No more data in response.');
        });
    }).end();*/
}

// Accept Ride Function
exports.acceptRide = function(req, res) {
    if( req.driver_id != null && req.ride_id != null ) {
        ride_model.acceptRide(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                // console.log(data);
                var return_response = {
                    'ride_detail': data
                }
                // Get Driver Information
                driver_model.getInfo(data.driver_id, function(err, dat) {
                    // console.log(dat);
                    if( err ) {
                        return res(err);
                    } else {
                        return_response.driver_detail = dat;
                        
                        // Get Passenger Information
                        passenger_model.getInfo(data.passenger_id, function(err, dat) {
                            if( err ) {
                                return res(err);
                            } else {
                                return_response.passenger_detail = dat;
        
                                return res(null, return_response);
                            }
                        });
                    }
                });
            }
        });
    } else {
        return res("Bad request");
    }
}

// Store Ride Route Function
exports.storeRideRoute = function(req, res) {
    if( req.ride_id != null && req.latitude != null && req.longitude != null ) {
        ride_model.storeRideRoute(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                // Get Passenger Information
                passenger_model.getSocketIdByRideId(req, function(err, dat) {
                    if( err ) {
                        return res(err);
                    } else {
                        data.passenger_socket_id = dat.passenger_socket_id;
                        return res(null, data);
                    }
                });
            }
        });
    } else {
        return res("Bad request");
    }
}


// Driver Arrived Function
exports.driverArrived = function(req, res) {
    if( req.ride_id != null ) {
        ride_model.driverArrived(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                // Get Passenger Socket ID
                passenger_model.getSocketIdByRideId(req, function(err, dat) {
                    if( err ) {
                        return res(err);
                    } else {
                        data.passenger_socket_id = dat.passenger_socket_id;
                        return res(null, data);
                    }
                });
            }
        });
    } else {
        return res("Bad request");
    }
}

// Ride Started Function
exports.rideStarted = function(req, res) {
    if( req.ride_id != null ) {
        ride_model.rideStarted(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                // Get Passenger Socket ID
                passenger_model.getSocketIdByRideId(req, function(err, dat) {
                    if( err ) {
                        return res(err);
                    } else {
                        data.passenger_socket_id = dat.passenger_socket_id;
                        return res(null, data);
                    }
                });
            }
        });
    } else {
        return res("Bad request");
    }
}

// Cancel Ride Function
exports.cancelRide = function(req, res) {
    if( req.ride_id != null && req.cancelled_by != null && req.cancellation_reason_id != null ) {
        ride_model.cancelRide(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                // Get Passenger Socket ID
                passenger_model.getSocketIdByRideId(req, function(err, dat) {
                    if( err ) {
                        return res(err);
                    } else {
                        data.passenger_socket_id = dat.passenger_socket_id;
                        // Get Driver Socket ID
                        driver_model.getSocketIdByRideId(req, function(err, dat) {
                            if( err ) {
                                return res(err);
                            } else {
                                data.driver_socket_id = dat.driver_socket_id;
                                data.ride_id = req.ride_id;
                                return res(null, data);
                            }
                        });
                    }
                });
            }
        });
    } else {
        return res("Bad request");
    }
}

// Ride Completed Function
exports.rideCompleted = function(req, res) {
    if( req.ride_id != null ) {
        ride_model.rideCompleted(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                // console.log(data);
                if( data.routes.length > 0 ) {
                    var tot_records = data.routes.length;
                    var tot_distance_in_miles = 0.00;
                    for( var i = 0; i < (tot_records-1); i++ ) {
                        tot_distance_in_miles += distanceBetween(data.routes[i].latitude, data.routes[i].longitude, data.routes[i+1].latitude, data.routes[i+1].longitude, 'M');
                    }
                    
                    var duration_in_minutes = differenceInMinutes(data.routes[0].datetime, data.routes[tot_records-1].datetime);
                    
                    // Calculate Waiting Time
                    var waiting_time_in_minutes = differenceInMinutes(data.waiting_start_time, data.waiting_end_time);

                    // Get Vehicle Type Details
                    vehicle_type_model.getInfo(data.vehicle_type_id, function(err, dat) {
                        if( err ) {
                            return res(err);
                        } else {
                            let actual_price = 0.00;
                            let duration_fee = 0.00;
                            let distance_fee = 0.00;
                            let waiting_fee = 0.00;
                            waiting_fee = (dat.per_minute_fare * waiting_time_in_minutes).toFixed(2);
                            duration_fee = (dat.per_minute_fare * duration_in_minutes).toFixed(2);
                            distance_fee = (dat.per_mile_fare * tot_distance_in_miles).toFixed(2);
                            actual_price = dat.base_fare + parseFloat(distance_fee) + parseFloat(duration_fee) + parseFloat(waiting_fee);
                            let waiting_time = waiting_time_in_minutes;
                            let actual_distance = (tot_distance_in_miles).toFixed(2);
                            let actual_duration = duration_in_minutes;

                            var response = {
                                ride_id: req.ride_id,
                                actual_price: actual_price,
                                actual_distance: actual_distance,
                                actual_duration: actual_duration,
                                base_fare: dat.base_fare,
                                duration_fee: duration_fee,
                                distance_fee: distance_fee,
                                waiting_fee: waiting_fee,
                                waiting_time: waiting_time
                            }

                            // console.log(JSON.stringify(response));

                            // UPDATE actual ride price
                            ride_model.updateRidePrice(response, function(err, dat) {
                                if( err ) {
                                    return res(err);
                                } else {
                                    // CHARGE Passenger Card if payment method is card
                                    passenger_model.chargePassenger(response, function(err, dat) {
                                        if( err ) {
                                            return res(err);
                                        } else {
                                            response.actual_price = '$ ' + response.actual_price;
                                            response.actual_distance = response.actual_distance + 'M';
                                            response.actual_duration = response.actual_duration + 'M';
                                            response.duration_fee = '$' + response.duration_fee;
                                            response.distance_fee = '$' + response.distance_fee;
                                            response.base_fare = '$' + response.base_fare;
                                            response.waiting_fee = '$' + response.waiting_fee;
                                            response.waiting_time = response.waiting_time + 'M';

                                            // Get Passenger Socket ID
                                            passenger_model.getSocketIdByRideId(req, function(err, dat) {
                                                if( err ) {
                                                    return res(err);
                                                } else {
                                                    response.passenger_socket_id = dat.passenger_socket_id;
                                                    return res(null, response);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });

                    // var coords = [];
                    // if( tot_records > 20 ) {
                    //     coords[0] = data.routes[0].latitude + ',' + data.routes[0].longitude;
                    //     var gap = Math.floor(tot_records / 20);
                    //     index = 0;
                    //     for( var i = 1; i <= 18; i++  ) {
                    //       coords[i] = data.routes[index+gap].latitude + ',' + data.routes[index+gap].longitude;
                    //       index += gap;
                    //     }
                    //     coords[19] = data.routes[tot_records-1].latitude + ',' + data.routes[tot_records-1].longitude
                
                    // } else {
                    //     for( var i = 0; i < data.routes.length; i++  ) {
                    //       coords[i] = data.routes[i].latitude + ',' + data.routes[i].longitude;
                    //     }
                    // }
                    
                    // var waypoints = '';
                    // for( var i = 0; i < coords.length; i++ )
                    // {
                    //     if( i == (coords.length-1) ) {
                    //         waypoints += 'via:'+coords[i];
                    //     } else {
                    //         waypoints += 'via:'+coords[i]+'|';
                    //     }
                    // }
                    
                    // request('https://maps.googleapis.com/maps/api/directions/json?origin='+coords[0]+'&destination='+coords[coords.length-1]+'&waypoints='+waypoints+'&key=AIzaSyCepqEimgEC5ZP-YSkYgvMnGROBKlw_DbM', function (error, response, body) {
                    //     // console.log('error:', error); // Print the error if one occurred
                    //     // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                    //     if( response && response.statusCode == 200 ) {
                    //         let bb = JSON.parse(body);
                    //         console.log('Distance:', bb.routes[0].legs[0].distance.text);
                    //         console.log('Distance:', bb.routes[0].legs[0].distance.value);
                    //         console.log('Duration:', bb.routes[0].legs[0].duration.text);
                    //         console.log('Duration:', bb.routes[0].legs[0].duration.value);

                    //         // Get Vehicle Type Details
                    //         vehicle_type_model.getInfo(data.vehicle_type_id, function(err, dat) {
                    //             if( err ) {
                    //                 return res(err);
                    //             } else {
                                    
                    //                 let actual_price = 0.00;
                    //                 let duration_fee = 0.00;
                    //                 let distance_fee = 0.00;
                    //                 duration_fee = dat.per_minute_fare * bb.routes[0].legs[0].duration.value;
                    //                 distance_fee = dat.per_mile_fare * bb.routes[0].legs[0].distance.value;
                    //                 actual_price = dat.base_fare + ( distance_fee ) + ( duration_fee );
                    //                 let actual_distance = bb.routes[0].legs[0].distance.text;
                    //                 let actual_duration = bb.routes[0].legs[0].duration.text;

                    //                 var response = {
                    //                     ride_id: req.ride_id,
                    //                     actual_price: actual_price,
                    //                     actual_distance: actual_distance,
                    //                     actual_duration: actual_duration,
                    //                     base_fare: dat.base_fare,
                    //                     duration_fee: duration_fee,
                    //                     distance_fee: distance_fee
                    //                 }

                    //                 // UPDATE actual ride price
                    //                 ride_model.updateRidePrice(response, function(err, dat) {
                    //                     if( err ) {
                    //                         return res(err);
                    //                     } else {

                    //                         // CHARGE Passenger Card if payment method is card
                    //                         passenger_model.chargePassenger(response, function(err, dat) {
                    //                             if( err ) {
                    //                                 return res(err);
                    //                             } else {
                    //                                 return res(null, response);
                    //                             }
                    //                         });
                    //                     }
                    //                 });
                    //             }
                    //         });

                    //     }
                    // });
                    
                } else {
                    console.log('no routes found in DB');
                }
                // return res(null, data);
            }
        });
    } else {
        return res("Bad request");
    }
}

function distanceBetween(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="K") { dist = dist * 1.609344 }
	if (unit=="N") { dist = dist * 0.8684 }
	return dist
}

function differenceInMinutes(start_date, end_date) {
    var diff = Math.abs(new Date(end_date) - new Date(start_date));
    var minutes = Math.floor((diff/1000)/60);
    return minutes;
}
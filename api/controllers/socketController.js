// driverController.js - Driver Controller module.
var config = require('../../config');
var http = require('http');
var bodyParser = require('body-parser');

// Require model modules.
var socket_model = require('../models/socket');

// Index Function
exports.index = function(req, res) {
    res.send('Invalid method');
}

// Store Connection Function
exports.storeConnection = function(req, res) {
    if( req.user_id != null && req.type != null && req.socket_id != null ) {
        socket_model.storeConnection(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                return res(null, data);
            }
        });
    } else {
        return res("Bad request");
    }
}

// Remove Connection Function
exports.removeConnection = function(req, res) {
    if( req.socket_id != null ) {
        socket_model.removeConnection(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                return res(null, data);
            }
        });
    } else {
        return res("Bad request");
    }
}
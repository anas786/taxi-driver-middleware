// driverController.js - Driver Controller module.
var config = require('../../config');
var http = require('http');
var bodyParser = require('body-parser');

// Require model modules.
var driver_model = require('../models/driver');

// Index Function
exports.index = function(req, res) {
    res.send('Invalid method');
}

// Log Location Function
exports.logLocation = function(req, res) {
    if( req.driver_id != null && req.latitude != null && req.longitude != null ) {
        driver_model.logLocation(req, function(err, data) {
            if( err ) {
                return res(err);
            } else {
                return res(null, data);
            }
        });
    } else {
        return res("Bad request");
    }
}
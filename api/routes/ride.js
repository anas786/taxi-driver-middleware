// ride.js - Ride route module.

var express = require('express');
var router = express.Router();

// Require controller modules.
var ride_controller = require('../controllers/rideController');

router.get('/', ride_controller.index);

router.post('/book', ride_controller.book_ride);

module.exports = router;
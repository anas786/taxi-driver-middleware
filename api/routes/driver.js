// driver.js - Driver route module.

var express = require('express');
var router = express.Router();

// Require controller modules.
var driver_controller = require('../controllers/driverController');

router.get('/', driver_controller.index);

router.post('/log_location', driver_controller.logLocation);

module.exports = router;
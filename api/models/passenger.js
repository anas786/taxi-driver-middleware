// passenger.js - Passenger Model module.
var config = require('../../config');
var mysql = require('mysql');
var stripe = require("stripe")(config.STRIPE_TEST_KEY);

// Table names
var tbl_passenger = 'tbl_passenger';
var tbl_passenger_option = 'tbl_passenger_option';
var tbl_passenger_card = 'tbl_passenger_card';
var tbl_ride = 'tbl_ride';
var tbl_connected_user = 'tbl_connected_user';
var tbl_ride_rating = 'tbl_ride_rating';

// Create MySQL Pool
var pool  = mysql.createPool({
    connectionLimit : 100, //important
    host     : config.mysql.host,
    user     : config.mysql.user,
    password : config.mysql.password,
    database : config.mysql.db
});

// Passenger Information Function
exports.getInfo = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var passenger_id = req;
            console.log('passenger id => ' + passenger_id);
            var quer = con.query('SELECT * FROM ?? WHERE `id` = ?', [tbl_passenger, passenger_id], function(err, results) {
                // console.log(quer.sql);
                if( err ) {
                    console.log(err);
                    return callback(err);
                } else {
                    //console.log(results.length);
                    var resp = results[0];
                    resp.photo = config.PASSENGER_PHOTO_PATH + resp.photo;
                    // Get User Socket ID
                    var type = 2;
                    var quer = con.query('SELECT `socket_id` FROM ?? WHERE `type` = ? AND `user_id` = ?', [tbl_connected_user, type, passenger_id], function(err, results) {
                        // console.log(quer.sql);
                        if( err ) {
                            console.log(err);
                            return callback(err);
                        } else {
                            if( results.length > 0 ) {
                                resp.socket_id = results[0].socket_id;
                            }
                            
                            // Passenger Rating
                            var quer = con.query('SELECT SUM(??.rating) AS `total_rating`, COUNT(??.id) AS `total_records` FROM ?? JOIN ?? ON ??.id = ??.ride_id WHERE ??.type = ? AND ??.passenger_id = ?', [tbl_ride_rating, tbl_ride_rating, tbl_ride_rating, tbl_ride, tbl_ride, tbl_ride_rating, tbl_ride_rating, 2, tbl_ride, passenger_id], function(err, results) {
                                // console.log(quer.sql);
                                if( err ) {
                                    console.log(err);
                                    return callback(err);
                                } else {
                                    if( results.length > 0 ) {
                                        rate = results[0];
                                        if( rate.total_records > 0 ) {
                                            resp.rating = (rate.total_rating / rate.total_records).toFixed(2);
                                        }
                                    }
                                    
                                    // Passenger Payment Method
                                    var quer = con.query('SELECT * FROM ?? WHERE option_key = "default_payment_method" AND passenger_id = ?', [tbl_passenger_option, passenger_id], function(err, results) {
                                        // console.log(quer.sql);
                                        if( err ) {
                                            console.log(err);
                                            return callback(err);
                                        } else {
                                            if( results.length > 0 ) {
                                                if( results[0].option_value == '0' ) {
                                                    con.release();
                                                    // CASH
                                                    resp.payment_method = 'Cash';
                                                    resp.payment_image = config.CARD_PHOTO_PATH + 'cash.png';
                                                    return callback(null, resp);
                                                } else {
                                                    // CREDIT CARD
                                                    var quer = con.query('SELECT * FROM ?? WHERE id = ?', [tbl_passenger_card, results[0].option_value], function(err, results) {
                                                        // console.log(quer.sql);
                                                        con.release();
                                                        if( err ) {
                                                            console.log(err);
                                                            return callback(err);
                                                        } else {
                                                            if( results.length > 0 ) {
                                                                resp.payment_method = 'Card';
                                                                resp.payment_image = config.CARD_PHOTO_PATH + results[0].card_type + '.png';
                                                                resp.payment_number = results[0].card_number.substr(results[0].card_number.length - 4);
                                                            }
                                                            return callback(null, resp);
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        
    });

}

// Get Passenger Socket ID
exports.getSocketIdByRideId = function(req, callback) {
    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var ride_id = req.ride_id;
            var quer = con.query('SELECT `passenger_id` FROM ?? WHERE `id` = ?', [tbl_ride, ride_id], function(err, results) {
                console.log(quer.sql);
                if( err ) {
                    return callback(err);
                } else {
                    var passenger_id = results[0].passenger_id;
                    // Get Socket ID
                    var query = con.query('SELECT `socket_id` FROM ?? WHERE `user_id` = ? AND `type` = 2', [tbl_connected_user, passenger_id], function(err, results) {
                        con.release();
                        if( err ) {
                            return callback(err);
                        } else {
                            var res = {
                                passenger_socket_id: results[0].socket_id
                            }
                            return callback(null, res);
                        }
                    });
                }
            });
        }
    });
}

// Charge Passenger Function
exports.chargePassenger = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var ride_id = req.ride_id;
            var query = con.query('SELECT * FROM ?? WHERE `id` = ?', [tbl_ride, ride_id], function(err, results) {
                // console.log(query.sql);
                if( err ) {
                    return callback(err);
                } else {
                    //console.log(results.length);
                    if( results[0].payment_method == '0' ) {
                        // CASH
                        return callback(null, results[0]);
                    } else {
                        // CARD
                        var query = con.query('SELECT * FROM ?? WHERE id = ?', [tbl_passenger_card, results[0].payment_method], function(err, results) {
                            con.release();
                            if( err ) {
                                return callback(err);
                            } else {
                                if( results.length > 0 ) {
                                    let cardd = results[0];
                                    // console.log(cardd);
                                    // Charge Card
                                    stripe.tokens.create({
                                        card: {
                                          "number": cardd.card_number,
                                          "exp_month": cardd.expiry_month,
                                          "exp_year": '20' + cardd.expiry_year,
                                          "cvc": cardd.cvv
                                        }
                                      }, function(err, token) {
                                            // console.log(token);
                                            // console.log(err);
                                            // asynchronously called
                                            // Create Charge
                                            let amm = (req.actual_price * 100).toFixed(0);
                                            stripe.charges.create({
                                                amount: amm,
                                                currency: "usd",
                                                source: token.id,
                                                description: "Charge for Ride ID: " + ride_id
                                            }, function(err, charge) {
                                                // console.log(err);
                                                console.log(charge);
                                                // let charge_id = charge.id
                                                // let desc = charge.description
                                                // asynchronously called
                                                return callback(null, results[0]);
                                            });
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
        
    });

}
// vehicle_type.js - Vehicle Type Model module.
var config = require('../../config');
var mysql = require('mysql');

// Table names
var tbl_vehicle_type = 'tbl_vehicle_type';

// Create MySQL Pool
var pool  = mysql.createPool({
    connectionLimit : 100, //important
    host     : config.mysql.host,
    user     : config.mysql.user,
    password : config.mysql.password,
    database : config.mysql.db
});

// Get Info Function
exports.getInfo = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var vehicle_type_id = req;
            var query = con.query('SELECT * FROM ?? WHERE `id` = ? AND `is_delete` = 0', [tbl_vehicle_type, vehicle_type_id], function(err, results) {
                con.release();
                // console.log(query.sql);
                if( err ) {
                    return callback(err);
                } else {
                    //console.log(results.length);
                    var resp = results[0];
                    return callback(null, resp);
                }
            });
        }
        
    });

}
// driver.js - Driver Model module.
var config = require('../../config');
var mysql = require('mysql');
var moment = require('moment-timezone');

// Table names
var tbl_ride = 'tbl_ride';
var tbl_ride_route = 'tbl_ride_route';
var tbl_ride_status = 'tbl_ride_status';
var tbl_ride_price = 'tbl_ride_price';
var tbl_driver = 'tbl_driver';
var tbl_ride_cancellation = 'tbl_ride_cancellation';

// Create MySQL Pool
var pool  = mysql.createPool({
    connectionLimit : 100, //important
    host     : config.mysql.host,
    user     : config.mysql.user,
    password : config.mysql.password,
    database : config.mysql.db
});

// Book Ride Function
exports.bookRide = function(req, callback) {
    console.log(config.mysql.password);
    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var timenow = moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss');
            var data = {
                passenger_id: req.passenger_id,
                passengers: req.passengers,
                vehicle_type_id: req.vehicle_type_id,
                pickup_coordinates: req.pickup_coordinates,
                pickup_location: req.pickup_location,
                dropoff_coordinates: req.dropoff_coordinates,
                dropoff_location: req.dropoff_location,
                payment_method: req.payment_method,
                ride_type: req.ride_type,
                date_add: timenow,
                date_update: timenow
            };
            var insert = con.query('INSERT INTO ?? SET ?', [tbl_ride, data], function(err, results) {
                if( err ) {
                    console.log(err);
                    return callback(err);
                } else {
                    data.ride_id = results.insertId;
                    // INSERT ride status
                    var dat = {
                        ride_id: data.ride_id,
                        status: 1,
                        datetime: timenow
                    }
                    var ins = con.query('INSERT INTO ?? SET ?', [tbl_ride_status, dat], function(err, res) {
                        if( err ) {
                            console.log(err);
                            return callback(err);
                        } else {
                            // INSERT ride price
                            var dat = {
                                ride_id: data.ride_id,
                                estimated_price: req.estimated_price,
                                estimated_duration: req.estimated_duration,
                                estimated_distance: req.estimated_distance
                            }
                            var ins = con.query('INSERT INTO ?? SET ?', [tbl_ride_price, dat], function(err, res) {
                                con.release();
                                if( err ) {
                                    console.log(err);
                                    return callback(err);
                                } else {
                                    return callback(null, data);
                                }
                            });
                        }
                    });
                    // return callback(null, data);
                }
            });
        }
        
    });

}

// Cancel Ride Function
exports.cancelRide = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var timenow = moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss');
            var data = {
                is_cancelled: 1,
                date_update: timenow
            };
            var insert = con.query('UPDATE ?? SET ?', [tbl_ride, data], function(err, results) {
                if( err ) {
                    return callback(err);
                } else {
                    // INSERT ride status
                    var dat = {
                        ride_id: req.ride_id,
                        status: 6,
                        datetime: timenow
                    }
                    var ins = con.query('INSERT INTO ?? SET ?', [tbl_ride_status, dat], function(err, res) {
                        if( err ) {
                            return callback(err);
                        } else {
                            // INSERT cancellation reason
                            var dat = {
                                ride_id: req.ride_id,
                                cancelled_by: req.cancelled_by,
                                cancellation_reason_id: req.cancellation_reason_id
                            }
                            var ins = con.query('INSERT INTO ?? SET ?', [tbl_ride_cancellation, dat], function(err, res) {
                                con.release();
                                if( err ) {
                                    console.log(err);
                                    return callback(err);
                                } else {
                                    return callback(null, data);
                                }
                            });
                        }
                    });
                    // return callback(null, data);
                }
            });
        }
        
    });

}

// Accept Ride Function
exports.acceptRide = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var timenow = moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss');
            // Check whether the ride is cancelled or already accepted by other driver
            var check = con.query('SELECT * FROM ?? WHERE `id` = ? AND (`is_cancelled` = 1 OR `is_accepted` = 1)', [tbl_ride, req.ride_id], function(err, results) {
                // console.log(check.sql);
                if( err ) {
                    console.log(err);
                    return callback(err);
                } else {
                    if( results.length > 0 ) {
                        var rr = {
                            status: false,
                            msg: 'Ride has been cancelled by Passenger'
                        }
                        return callback(null, rr);
                    } else {
                        // Not cancelled, good to go
                        // Create random channel name
                        var channel = 'channel_' + moment.now();
                        var data = {
                            driver_id: req.driver_id,
                            channel: channel,
                            is_accepted: 1,
                            date_update: timenow
                        };
                        var insert = con.query('UPDATE ?? SET ? WHERE `id` = ?', [tbl_ride, data, req.ride_id], function(err, results) {
                            if( err ) {
                                console.log(err);
                                return callback(err);
                            } else {
                                // INSERT ride status
                                var dat = {
                                    ride_id: req.ride_id,
                                    status: 2,
                                    datetime: timenow
                                }
                                var ins = con.query('INSERT INTO ?? SET ?', [tbl_ride_status, dat], function(err, res) {
                                    if( err ) {
                                        console.log(err);
                                        return callback(err);
                                    } else {
                                        // Status UPDATED
                                        // Update Driver as Busy
                                        var dat = {
                                            is_busy: 0
                                        }
                                        var upd = con.query('UPDATE ?? SET ? WHERE `id` = ?', [tbl_driver, dat, req.driver_id], function(err, res) {
                                            if( err ) {
                                                console.log(err);
                                                return callback(err);
                                            } else {
                                                // Get Ride Details
                                                var select = con.query('SELECT * FROM ?? WHERE `id` = ?', [tbl_ride, req.ride_id], function(err, results) {
                                                    if( err ) {
                                                        console.log(err);
                                                        return callback(err);
                                                    } else {
                                                        var ride_detail = results[0];
                                                        ride_detail.status = true;
                                                        // Get Ride Prices
                                                        var select = con.query('SELECT * FROM ?? WHERE `ride_id` = ?', [tbl_ride_price, req.ride_id], function(err, results) {
                                                            con.release();
                                                            if( err ) {
                                                                console.log(err);
                                                                return callback(err);
                                                            } else {
                                                                ride_detail.estimated_price = results[0].estimated_price;
                                                                ride_detail.estimated_duration = results[0].estimated_duration;
                                                                ride_detail.estimated_distance = results[0].estimated_distance;
                                                                return callback(null, ride_detail);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });
            
        }
        
    });

}

// Store Ride Route Function
exports.storeRideRoute = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var timenow = moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss');
            var data = {
                ride_id: req.ride_id,
                latitude: req.latitude,
                longitude: req.longitude,
                datetime: timenow
            };
            var insert = con.query('INSERT INTO ?? SET ?', [tbl_ride_route, data], function(err, results) {
                con.release();
                if( err ) {
                    // console.log(err);
                    return callback(err);
                } else {
                    return callback(null, data);
                }
            });
        }
        
    });

}

// Driver Arrived Function
exports.driverArrived = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var timenow = moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss');
            var data = {
                ride_id: req.ride_id,
                status: 3,
                datetime: timenow
            };
            var insert = con.query('INSERT INTO ?? SET ?', [tbl_ride_status, data], function(err, results) {
                con.release();
                if( err ) {
                    // console.log(err);
                    return callback(err);
                } else {
                    return callback(null, data);
                }
            });
        }
        
    });

}

// Ride Started Function
exports.rideStarted = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var timenow = moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss');
            var data = {
                ride_id: req.ride_id,
                status: 4,
                datetime: timenow
            };
            var insert = con.query('INSERT INTO ?? SET ?', [tbl_ride_status, data], function(err, results) {
                con.release();
                if( err ) {
                    // console.log(err);
                    return callback(err);
                } else {
                    return callback(null, data);
                }
            });
        }
        
    });

}


// Ride Completed Function
exports.rideCompleted = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var timenow = moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss');
            var data = {
                ride_id: req.ride_id,
                status: 5,
                datetime: timenow
            };
            var insert = con.query('INSERT INTO ?? SET ?', [tbl_ride_status, data], function(err, results) {
                if( err ) {
                    // console.log(err);
                    return callback(err);
                } else {
                    
                    // Mark Ride as Completed
                    var dat = {
                        is_completed: 1
                    }
                    var insert = con.query('UPDATE ?? SET ? WHERE `id` = ?', [tbl_ride, dat, req.ride_id], function(err, results) {
                        if( err ) {
                            // console.log(err);
                            return callback(err);
                        } else {
                            
                            // Mark Driver FREE
                            var select = con.query('SELECT * FROM ?? WHERE `id` = ?', [tbl_ride, req.ride_id], function(err, results) {
                                if( err ) {
                                    // console.log(err);
                                    return callback(err);
                                } else {
                                    var dat = {
                                        is_busy: 0
                                    }
                                    var upd = con.query('UPDATE ?? SET ? WHERE `id` = ?', [tbl_driver, dat, results[0].driver_id], function(err, res) {
                                        if( err ) {
                                            // console.log(err);
                                            return callback(err);
                                        } else {
                                            // GET ride info
                                            var rt = con.query('SELECT * FROM ?? WHERE `id` = ?', [tbl_ride, req.ride_id], function(err, res) {
                                                if( err ) {
                                                    // console.log(err);
                                                    return callback(err);
                                                } else {
                                                    var resp = {
                                                        vehicle_type_id: res[0].vehicle_type_id
                                                    }
                                                    // GET ride routes
                                                    var rt = con.query('SELECT * FROM ?? WHERE `ride_id` = ?', [tbl_ride_route, req.ride_id], function(err, res) {
                                                        if( err ) {
                                                            // console.log(err);
                                                            return callback(err);
                                                        } else {
                                                            resp.routes = res;
                                                            // Get ride statuses
                                                            var rt = con.query('SELECT * FROM ?? WHERE `ride_id` = ? AND status IN (3,4)', [tbl_ride_status, req.ride_id], function(err, res) {
                                                                con.release();
                                                                if( err ) {
                                                                    // console.log(err);
                                                                    return callback(err);
                                                                } else {
                                                                    resp.waiting_start_time = res[0].datetime;
                                                                    resp.waiting_end_time = res[1].datetime;
                                                                    return callback(null, resp);
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                            
                                        }
                                    });
                                }
                            });

                        }
                    });
                }
            });
        }
        
    });

}

// Update Ride Price Function
exports.updateRidePrice = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var data = {
                actual_price: req.actual_price,
                actual_distance: req.actual_distance,
                actual_duration: req.actual_duration,
                waiting_time: req.waiting_time
            };
            var insert = con.query('UPDATE ?? SET ? WHERE `ride_id` = ?', [tbl_ride_price, data, req.ride_id], function(err, results) {
                // console.log(insert.sql);
                con.release();
                if( err ) {
                    // console.log(err);
                    return callback(err);
                } else {
                    return callback(null, data);
                }
            });
        }
        
    });

}
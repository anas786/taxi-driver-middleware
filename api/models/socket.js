// socket.js - Socket Model module.
var config = require('../../config');
var mysql = require('mysql');

// Table names
var tbl_connection = 'tbl_connected_user';

// Create MySQL Pool
var pool  = mysql.createPool({
    connectionLimit : 100, //important
    host     : config.mysql.host,
    user     : config.mysql.user,
    password : config.mysql.password,
    database : config.mysql.db
});

// Store Connection Function
exports.storeConnection = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            // Delete old connection, if exists
            var user_id = req.user_id;
            var type = req.type;
            var query = con.query('DELETE FROM ?? WHERE `user_id` = ? AND `type` = ?', [tbl_connection, user_id, type], function(err, results) {
                //console.log(query.sql);
                if( err ) {
                    return callback(err);
                } else {
                    // INSERT new connection
                    var data = {
                        user_id: user_id,
                        type: type,
                        socket_id: req.socket_id
                    };
                    var insert = con.query('INSERT INTO ?? SET ?', [tbl_connection, data], function(err, results) {
                        con.release();
                        if( err ) {
                            return callback(err);
                        } else {
                            return callback(null, data);
                        }
                    });
                }
            });
        }
    });
}

// Remvoe Connection Function
exports.removeConnection = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            // Delete connection
            var socket_id = req.socket_id;
            var query = con.query('DELETE FROM ?? WHERE `socket_id` = ?', [tbl_connection, socket_id,], function(err, results) {
                con.release();
                //console.log(query.sql);
                if( err ) {
                    return callback(err);
                } else {
                    return callback(null, "Removed");
                }
            });
        }
    });
}
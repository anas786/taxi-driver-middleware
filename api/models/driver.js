// driver.js - Driver Model module.
var config = require('../../config');
var mysql = require('mysql');

// Table names
var tbl_driver = 'tbl_driver';
var tbl_ride = 'tbl_ride';
var tbl_driver_location = 'tbl_online_driver';
var tbl_connected_user = 'tbl_connected_user';
var tbl_ride_rating = 'tbl_ride_rating';
var tbl_driver_vehicle = 'tbl_driver_vehicle';
var tbl_vehicle_make_model = 'tbl_vehicle_make_model';
var tbl_color = 'tbl_color';

// Create MySQL Pool
var pool  = mysql.createPool({
    connectionLimit : 100, //important
    host     : config.mysql.host,
    user     : config.mysql.user,
    password : config.mysql.password,
    database : config.mysql.db
});

// Log Location Function
exports.logLocation = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            // Check if driver is online
            var driver_id = req.driver_id;
            var query = con.query('SELECT * FROM ?? WHERE `driver_id` = ?', [tbl_driver_location, driver_id], function(err, results) {
                // console.log(query.sql);
                if( err ) {
                    return callback(err);
                } else {
                    //console.log(results.length);
                    if( results.length == 0 ) {
                        // Offline, INSERT
                        var data = {
                            driver_id: driver_id,
                            latitude: req.latitude,
                            longitude: req.longitude
                        };
                        var insert = con.query('INSERT INTO ?? SET ?', [tbl_driver_location, data], function(err, results) {
                            con.release();
                            if( err ) {
                                return callback(err);
                            } else {
                                return callback(null, data);
                            }
                        });
                    } else {
                        // Online, UPDATE
                        var data = {
                            latitude: req.latitude,
                            longitude: req.longitude
                        };
                        var insert = con.query('UPDATE ?? SET ? WHERE `driver_id` = ?', [tbl_driver_location, data, driver_id], function(err, results) {
                            con.release();
                            if( err ) {
                                return callback(err);
                            } else {
                                return callback(null, data);
                            }
                        });
                    }
                }
            });
        }

    });
}

// Get Driver Socket ID
exports.getSocketIdByRideId = function(req, callback) {
    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var ride_id = req.ride_id;
            var quer = con.query('SELECT `driver_id` FROM ?? WHERE `id` = ?', [tbl_ride, ride_id], function(err, results) {
                // console.log(quer.sql);
                if( err ) {
                    return callback(err);
                } else {
                    var driver_id = results[0].driver_id;
                    // Get Socket ID
                    var query = con.query('SELECT `socket_id` FROM ?? WHERE `user_id` = ? AND `type` = 1', [tbl_connected_user, driver_id], function(err, results) {
                        con.release();
                        if( err ) {
                            return callback(err);
                        } else {
                            var res = {
                                driver_socket_id: results[0].socket_id
                            }
                            return callback(null, res);
                        }
                    });
                }
            });
        }
    });
}

// Driver Information Function
exports.getInfo = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            var driver_id = req;
            var quer = con.query('SELECT * FROM ?? WHERE `id` = ?', [tbl_driver, driver_id], function(err, results) {
                console.log(quer.sql);
                if( err ) {
                    return callback(err);
                } else {
                    //console.log(results.length);
                    var resp = results[0];
                    resp.photo = config.DRIVER_PHOTO_PATH + resp.photo;
                    // Get User Socket ID
                    var type = 1;
                    var query = con.query('SELECT `socket_id` FROM ?? WHERE `type` = ? AND `user_id` = ?', [tbl_connected_user, type, driver_id], function(err, results) {
                        // console.log(query.sql);
                        if( err ) {
                            return callback(err);
                        } else {
                            if( results.length > 0 ) {
                                resp.socket_id = results[0].socket_id;
                            }
                            
                            // Get Current Location, if online
                            var query = con.query('SELECT `latitude`, `longitude` FROM ?? WHERE `driver_id` = ?', [tbl_driver_location, driver_id], function(err, results) {
                                if( err ) {
                                    return callback(err);
                                } else {
                                    if( results.length > 0 ) {
                                        resp.driver_location_latitude = results[0].latitude;
                                        resp.driver_location_longitude = results[0].longitude;
                                    }

                                    // Driver Rating
                                    var query = con.query('SELECT SUM(??.rating) AS `total_rating`, COUNT(??.id) AS `total_records` FROM ?? JOIN ?? ON ??.id = ??.ride_id WHERE ??.type = ? AND ??.driver_id = ?', [tbl_ride_rating, tbl_ride_rating, tbl_ride_rating, tbl_ride, tbl_ride, tbl_ride_rating, tbl_ride_rating, 1, tbl_ride, driver_id], function(err, results) {
                                        // console.log(query.sql);
                                        if( err ) {
                                            return callback(err);
                                        } else {
                                            if( results.length > 0 ) {
                                                rate = results[0];
                                                if( rate.total_records > 0 ) {
                                                    resp.rating = (rate.total_rating / rate.total_records).toFixed(2);
                                                }
                                            }

                                            // Driver Vehicle Info
                                            var query = con.query('SELECT ??.`plate_number`, ??.`make`, ??.`model`, ??.`year`, ??.`name` AS `color` FROM ?? JOIN ?? ON ??.`id`=??.`vehicle_make_model_id` JOIN ?? ON ??.`id`=??.`color_id` WHERE ??.`driver_id` = ? AND ??.`is_delete` = ? AND ??.`is_active` = ?', [tbl_driver_vehicle, tbl_vehicle_make_model, tbl_vehicle_make_model, tbl_vehicle_make_model, tbl_color, tbl_driver_vehicle, tbl_vehicle_make_model, tbl_vehicle_make_model, tbl_driver_vehicle, tbl_color, tbl_color, tbl_driver_vehicle, tbl_driver_vehicle, driver_id, tbl_driver_vehicle, 0, tbl_driver_vehicle, 1], function(err, results) {
                                                // console.log(query.sql);
                                                con.release();
                                                if( err ) {
                                                    return callback(err);
                                                } else {
                                                    if( results.length > 0 ) {
                                                        resp.vehicle = results[0];
                                                    }
                                                    return callback(null, resp);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        
    });

}

// Find Nearby Drivers Function
exports.findNearbyDrivers = function(req, callback) {

    pool.getConnection(function(err, con) {
        if(err) {
            return callback(err);
        } else {
            // Find nearby online drivers 
            var pickup_coordinates = req.pickup_coordinates.split(",");
            var distance = 1000;  // 15 miles
            var query = con.query('SELECT ??.id, ??.driver_id, ( 3959 * acos( cos( radians(?) ) * cos( radians( ??.latitude ) ) * cos( radians( ??.longitude ) - radians(?) ) + sin( radians(?) ) * sin( radians( ??.latitude ) ) ) ) AS distance FROM ?? JOIN ?? ON ??.id = ??.`driver_id` WHERE ??.is_busy = 0 GROUP BY ??.driver_id HAVING distance < ? ORDER BY distance', [tbl_driver_location, tbl_driver_location, pickup_coordinates[0], tbl_driver_location, tbl_driver_location, pickup_coordinates[1], pickup_coordinates[0], tbl_driver_location, tbl_driver_location, tbl_driver, tbl_driver, tbl_driver_location, tbl_driver, tbl_driver_location, distance], function(err, results) {
                console.log(query.sql);
                if( err ) {
                    return callback(err);
                } else {
                    if( results.length > 0 ) {
                        // Found drivers, get socket ids
                        getSocketIDs(0, 1, results, callback);
                    } else {
                        // No driver found
                        return callback("No driver is available at the moment");
                    }
                }
            });
        }

        var socket_ids = [];
        function getSocketIDs(index, type, results, callback) {
            var type = type;
            var user_id = results[index].driver_id;
            var sockets = con.query('SELECT socket_id FROM ?? WHERE `type` = ? AND `user_id` = ?', [tbl_connected_user, type, user_id], function(err, res) {
                console.log(sockets.sql);
                if( err ) {
                    return callback(err);
                } else {
                    if( res.length > 0 ) {
                        socket_ids.push(res[0].socket_id);
                        index++;
                        if( index < results.length ) {
                            getSocketIDs(index, type, results, callback);
                        } else {
                            con.release();
                            return callback(null, socket_ids);
                        }
                    } else {
                        index++;
                        if( index < results.length ) {
                            getSocketIDs(index, type, results, callback);
                        } else {
                            con.release();
                            return callback(null, socket_ids);
                        }
                    }
                    
                }
            });
        }
        
    });
    

}
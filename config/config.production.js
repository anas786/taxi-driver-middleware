var config = require('./config.global');

config.env = 'production';
config.hostname = 'production.taxiapp';

//mysql database
config.mysql = {};
config.mysql.host = 'localhost';
config.mysql.user = 'root';
config.mysql.password = 'digitalocean2011@';
config.mysql.db = 'nvoii';

// API Server Config
config.API_SERVER = 'http://45.55.174.194';
config.API_PATH = '/nvoii_manager/api/v1';
config.API_PORT = 80;

// Stripe Details
config.STRIPE_TEST_KEY = 'sk_test_wCgMUyCGTPADznTSieHLSw55';
config.STRIPE_LIVE_KEY = 'sk_live_OlkMJXFRvU8zVHQfQdUQqxC1';

// Images Paths
config.PASSENGER_PHOTO_PATH = config.API_SERVER + '/nvoii_manager/assets/uploads/passengers/';
config.DRIVER_PHOTO_PATH = config.API_SERVER + '/nvoii_manager/assets/uploads/drivers/';
config.CARD_PHOTO_PATH = config.API_SERVER + '/nvoii_manager/assets/uploads/cards/';

module.exports = config;
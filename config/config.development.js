var config = require('./config.global');

config.env = 'development';
config.hostname = 'development.taxiapp';

//mysql database
config.mysql = {};
config.mysql.host = 'localhost';
config.mysql.user = 'admin';
config.mysql.password = 'admin';
config.mysql.db = 'ridenow';

// API Server Config
config.API_SERVER = 'localhost';
config.API_PATH = '/ridenow_manager/api/v1';
config.API_PORT = 80;

// Stripe Details
config.STRIPE_TEST_KEY = 'sk_test_wCgMUyCGTPADznTSieHLSw55';
config.STRIPE_LIVE_KEY = 'sk_live_OlkMJXFRvU8zVHQfQdUQqxC1';

// Images Paths
config.PASSENGER_PHOTO_PATH = config.API_SERVER + '/nvoii_manager/assets/uploads/passengers/';
config.DRIVER_PHOTO_PATH = config.API_SERVER + '/nvoii_manager/assets/uploads/drivers/';
config.CARD_PHOTO_PATH = config.API_SERVER + '/nvoii_manager/assets/uploads/cards/';

module.exports = config;
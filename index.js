var config = require('./config');
var app = require('express')();
var http = require('http').Server(app);
var bodyParser = require('body-parser');
var Socket = require('socket.io');
var moment = require('moment-timezone');
var pingInterval = 25000;
var io = Socket(http, {'pingInterval': pingInterval, 'pingTimeout': 60000});
//var ride = require('./api/routes/ride');
//var driver = require('./api/routes/driver');
var rideController = require('./api/controllers/rideController');
var driverController = require('./api/controllers/driverController');
var socketController = require('./api/controllers/socketController');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//app.use('/ride', ride);
//app.use('/driver', driver);

/**
 * You can control those variables as you want
 */

var serverPort = 3000;
var app_key_secret = "NvOiIApp";
var debugging_mode = true;

/*
  Server listener
 */
var port = process.env.PORT || serverPort;
http.listen(port, function () {
    console.log('Server listening at port %d', port);
});

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

/**
 * this for check if the user connect from the app
 */
io.use(function (socket, next) {
    var token = socket.handshake.query.token;
    if (token === app_key_secret) {
        if (debugging_mode) {
            console.log("Token Valid  Authorized", token);
        }
        next();
    } else {
        if (debugging_mode) {
            console.log("not a valid token Unauthorized to access ");
        }
        next(new Error("Invalid Token"));
    }
});
io.on('connection', function(socket) {
    console.log('a user connected with ID: ', socket.id);

    /*
     * ===============================
     * Log Driver Location
     * @type JSON
     * @request { driver_id: 4, latitude: 12345, longitude: 12345 }
     * ===============================
     */
    socket.on('log_location', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => log_location => ' + data);
        }
        var dat = JSON.parse(data);
        driverController.logLocation(dat, function(err, resp) {
            if( err ) {
                
            } else {
                socket.emit('log_location', resp);
            }
        });
    });

    /*
     * ===============================
     * Store User Connection
     * @type JSON
     * @request { user_id: 4, type: 1, socket_id: Ax98Hs95jsd889dn3 }
     * type 1 = Driver, 2 = Passenger
     * ===============================
     */
    socket.on('store_connection', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => store_connection => ' + data);
        }
        var dat = JSON.parse(data);
        socketController.storeConnection(dat, function(err, resp) {
            if( err ) {
                socket.emit('store_connection', err);
            } else {
                socket.emit('store_connection', resp);
            }
        });
    });

    /*
     * ===============================
     * Remove User Connection
     * ===============================
     */
    socket.on('disconnect', function() {
        var dat = {
            socket_id: socket.id
        }
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => disconnect => ' + dat);
        }
        socketController.removeConnection(dat, function(err, resp) {
            if( err ) {
                
            } else {
                //socket.emit('store_connection', resp);
            }
        });
    });

    /*
     * ===============================
     * Book Ride
     * @type JSON
     * @request {
            passenger_id: 10,
            passengers: 2,
            vehicle_type_id: 1,
            pickup_coordinates: '24.02320382,64.12312312',
            pickup_location: 'Gulshan-e-Iqbal, Karachi, Pakistan',
            dropoff_coordinates: '24.34341342,64.283737372',
            dropoff_location: 'Aisha Manzil, Karachi, Pakistan',
            payment_method: 1
        }
     * payment_method 1 = Cash, 2 = Credit/Debit Card
     * ===============================
     */
    socket.on('book_ride', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => book_ride => ' + data);
        }
        var dat = JSON.parse(data);
        rideController.bookRide(dat, function(err, resp) {
            if( err ) {
                
            } else {
                socket.emit('book_ride', resp.ride_detail);
                for( var i = 0; i < resp.socket_ids.length; i++ ) {
                    // Emit ride request to each Driver Socket
                    console.log(resp.socket_ids[i]);
                    socket.to(resp.socket_ids[i]).emit('ride_request', resp.ride_detail);
                }
            }
        });
    });

    /*
     * ===============================
     * Ride Accept - Call when Driver accepts job request
     * @type JSON
     * @request {
            ride_id: 36,
            driver_id: 2
        }
     * ===============================
     */
    socket.on('ride_accept', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => ride_accept => ' + data);
        }
        var dat = JSON.parse(data);
        rideController.acceptRide(dat, function(err, resp) {
            if( err ) {
                console.log(err);
            } else {
                // console.log(JSON.stringify(resp));
                // Add Driver to Private Channel/Room
                // socket.join(resp.ride_detail.channel);
                // socket.to(resp.driver_detail.socket_id).emit('ride_accepted', resp);
                socket.emit('ride_accepted', resp);
                socket.to(resp.passenger_detail.socket_id).emit('ride_accepted', resp);
                // console.log(io.sockets.adapter.rooms);
            }
        });
    });


    /*
     * ===============================
     * Driver Arrived - Call when Driver arrived at pickup location
     * @type JSON
     * @request {
            ride_id: 36,
            channel: 'A_sdaljksmS8'
        }
     * ===============================
     */
    socket.on('driver_arrived', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => driver_arrived => ' + data);
        }
        var dat = JSON.parse(data);
        rideController.driverArrived(dat, function(err, resp) {
            if( err ) {
                
            } else {
                //  console.log(dat.channel);
                socket.to(resp.passenger_socket_id).emit('driver_arrived', resp);
                // console.log(io.sockets.adapter.rooms);
            }
        });
    });


    /*
     * ===============================
     * Ride Started - Call when Ride started by driver
     * @type JSON
     * @request {
            ride_id: 36,
            channel: 'A_sdaljksmS8'
        }
     * ===============================
     */
    socket.on('ride_started', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => ride_started => ' + data);
        }
        var dat = JSON.parse(data);
        rideController.rideStarted(dat, function(err, resp) {
            if( err ) {
                
            } else {
                // console.log(JSON.stringify(resp));
                socket.to(resp.passenger_socket_id).emit('ride_started', resp);
                // console.log(io.sockets.adapter.rooms);
            }
        });
    });

    /*
     * ===============================
     * Ride Completed - Call when Ride is completed
     * @type JSON
     * @request {
            ride_id: 36,
            channel: 'A_sdaljksmS8'
        }
     * ===============================
     */
    socket.on('ride_completed', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => ride_completed => ' + data);
        }
        var dat = JSON.parse(data);
        rideController.rideCompleted(dat, function(err, resp) {
            if( err ) {
                
            } else {
                // console.log(JSON.stringify(resp));
                // console.log(io.sockets.adapter.rooms[dat.channel].sockets);
                socket.emit('ride_completed', resp);
                socket.to(resp.passenger_socket_id).emit('ride_completed', resp);
                // console.log(io.sockets.adapter.rooms);
            }
        });
    });

    /*
     * ===============================
     * Cancel Ride - Cancel Ride during Journey
     * @type JSON
     * @request {
            ride_id: 36,
            cancelled_by: 1,
            cancellation_reason_id: 2
        }
     * ===============================
     */
    socket.on('cancel_ride', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => cancel_ride => ' + data);
        }
        var dat = JSON.parse(data);
        rideController.cancelRide(dat, function(err, resp) {
            if( err ) {
                console.log(err);
            } else {
                // socket.emit('cancel_ride', resp);
                socket.to(resp.driver_socket_id).emit('cancel_ride', resp);
                socket.to(resp.passenger_socket_id).emit('cancel_ride', resp);
            }
        });
    });


    /*
     * ===============================
     * Broadcast Location - Driver will broadcast his location during ride to the channel after every 2 seconds
     * @type JSON
     * @request {
            ride_id: 36,
            channel: 'kSMsa8saZSNsas9fm',
            latitude: 24.02320382,
            longitude: 64.12312312
        }
     * ===============================
     */
    socket.on('broadcast_location', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => broadcast_location => ' + data);
        }
        var dat = JSON.parse(data);
        rideController.storeRideRoute(dat, function(err, resp) {
            if( err ) {
                
            } else {
                // console.log(JSON.stringify(resp));
                socket.to(resp.passenger_socket_id).emit('broadcast_location', resp);
            }
        });
    });


    /*
     * ===============================
     * Join Channel - Call to Join some Room/Channel
     * @type JSON
     * @request {
            channel: "channel_120387120381"
        }
     * ===============================
     */
    socket.on('join_channel', function(data) {
        if( debugging_mode ) {
            console.log('[' + moment.tz(moment.now(), 'GMT').format('YYYY-MM-DD HH:mm:ss') + '] => join_channel => ' + data);
        }
        var dat = JSON.parse(data);
        socket.join(dat.channel);
    });

    socket.on('pass_to_socket', function(data) {
        var lat = 24.921883;
        var lng = 67.065521;
        var dat = JSON.parse(data);
        setInterval(function() {
            var resp = {
                ride_id: 5,
                latitude: lat,
                longitude: lng
            }
            socket.to(dat.socket_id).emit('broadcast_location', resp);
            lat += 0.000100;
            lng += 0.000100;
        }, 3000);
    });
    

});
